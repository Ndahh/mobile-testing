<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>e-commerce</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>aad236bd-5694-409d-a813-c0416ecebcff</testSuiteGuid>
   <testCaseLink>
      <guid>3b3183d6-76c6-449a-9807-16d6b6c01c62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/001_viewProduct</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8156d4ae-835c-4ef3-87f8-8fbb9f7f4812</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/002_addToCart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e535aaee-11e3-46d3-8317-39b0d0c5607e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/003_howToOrder</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
